import 'package:flutter/material.dart';

var theme = ThemeData(
    textTheme: TextTheme(
  titleLarge: TextStyle(
      color: Color.fromARGB(255, 58, 58, 58),
      fontWeight: FontWeight.w700,
      fontSize: 24,
      fontFamily: "Roboto"),
  titleMedium: TextStyle(
      color: Color.fromARGB(255, 129, 129, 129),
      fontWeight: FontWeight.w500,
      fontSize: 14,
      fontFamily: "Roboto"),
  titleSmall: TextStyle(
      color: Color.fromARGB(255, 255, 255, 255),
      fontWeight: FontWeight.w700,
      fontSize: 16,
      fontFamily: "Roboto"),
  ),
  filledButtonTheme: FilledButtonThemeData(
      style: FilledButton.styleFrom(
          textStyle: const TextStyle(fontWeight: FontWeight.bold),
          padding: const EdgeInsets.symmetric(horizontal: 36, vertical: 15),
          backgroundColor: Color.fromARGB(255, 106, 139, 249),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4)
          ),
          disabledBackgroundColor: Color.fromARGB(255, 129, 129, 129),
          disabledForegroundColor: Color.fromARGB(255, 255, 255, 255)
      )
  ),
);

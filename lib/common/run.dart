import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ses_sion_1_4/common/theme.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../auth/presintation/pages/signUpPage.dart';

void main() async{
  await Supabase.initialize(
      url: "https://czdvkuhvrfoyxllivtkw.supabase.co",
      anonKey: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImN6ZHZrdWh2cmZveXhsbGl2dGt3Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDkzOTYyNDcsImV4cCI6MjAyNDk3MjI0N30.D5ai7MeJKbAJkny_f-QCOC0GRE2r9G2moWlH9vsQMOA"
  );
  runApp(const MyApp());
}

class MyApp extends StatefulWidget{
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SignUpPage(),
      theme: theme,
    );
  }
}
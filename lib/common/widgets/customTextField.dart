import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class CustomTextField extends StatefulWidget{
  final String label;
  final String hint;
  final TextEditingController controller;
  final bool enableObscure;
  final Function(String)? onChange;

  const CustomTextField({
    super.key,
    required this.label,
    required this.hint,
    required this.controller,
    this.enableObscure = false,
    this.onChange});

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {

  bool isValue = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 24,),
        Text(widget.label,
        style: Theme.of(context).textTheme.titleMedium),
        SizedBox(height: 8),
        TextField(
          obscuringCharacter: "*",
          controller: widget.controller,
          onChanged: widget.onChange,
          decoration: InputDecoration(
            hintText: widget.hint,
            border: const OutlineInputBorder(
              borderSide: BorderSide(
                width: 1,
                  color: Color.fromARGB(255, 129, 129, 129)
              ),
            ),
            hintStyle: Theme.of(context).textTheme.titleMedium,
            contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 14),
            suffixIcon: (widget.enableObscure) ?
                GestureDetector(
                  onTap: (){
                    setState(() {
                      isValue = !isValue;
                    });
                  },
                  child: Image.asset("assets/eye-splash.img"),
                ) : null
          ),
        )
      ],
    );
  }
}